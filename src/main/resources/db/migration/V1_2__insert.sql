insert into public.tbl_investor (address, dob, email_id, first_name, last_name, mobile_number) values ('123 str OMAHA,Nebraska','1950-09-14', 'john_connor_gmail.com','John','Connor', 989389893);
insert into public.tbl_investor (address, dob, email_id, first_name, last_name, mobile_number) values ('123 str OMAHA,Newark','2010-09-01', 'emily_millers@gmail.com','Emily','Millers', 8987282);
insert into public.tbl_product (current_balance, investor_id, product_name, product_type) values (100, 1, 'Retire In Style', 'Retirement');
insert into public.tbl_product (current_balance, investor_id, product_name, product_type) values (90000, 1, 'Retire Care', 'Retirement');
insert into public.tbl_product (current_balance, investor_id, product_name, product_type) values (90000, 2, 'Retire In Style', 'Retirement');
