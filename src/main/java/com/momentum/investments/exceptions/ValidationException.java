package com.momentum.investments.exceptions;

public class ValidationException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	public String message;

	public ValidationException(String message) {
		this.message = message;
	}

	@Override
	public String getMessage() {
		return message;
	}
}
