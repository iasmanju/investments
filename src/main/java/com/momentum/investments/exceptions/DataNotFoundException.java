package com.momentum.investments.exceptions;

public class DataNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	private String message;

	public DataNotFoundException(String message) {
		this.message = message;
	}

	@Override
	public String getMessage() {
		return message;
	}
}
