package com.momentum.investments.model;

import java.math.BigInteger;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "tbl_investor", schema = "public")
public class Investor {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer investorId;
	private String firstName;
	private String lastName;
	private LocalDate dob;
	private String address;
	private BigInteger mobileNumber;
	private String emailId;
}
