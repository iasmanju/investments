package com.momentum.investments.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.momentum.investments.model.Investor;
public interface InvestorDetails extends JpaRepository<Investor, Integer> {

}
