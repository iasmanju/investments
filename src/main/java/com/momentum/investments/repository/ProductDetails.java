package com.momentum.investments.repository;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.momentum.investments.model.Product;

public interface ProductDetails extends JpaRepository<Product, Integer>{

	@Query(nativeQuery = true,value = "select * from tbl_product where investor_id=?1")
	public ArrayList<Product> getProductsByInvestord(int investorId);

}
