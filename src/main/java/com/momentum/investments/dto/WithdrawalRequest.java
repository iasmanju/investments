package com.momentum.investments.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class WithdrawalRequest {
	private int investorId;
	private int productId;
	private float amount;
}