package com.momentum.investments.dto;

import java.util.ArrayList;

import com.momentum.investments.model.Investor;
import com.momentum.investments.model.Product;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class CompleteInvestorDetails {
	private Investor investor;
	private ArrayList<Product> products;
}
