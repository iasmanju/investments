package com.momentum.investments.constants;

public class InvestmentResponseMessages {

	public static final String INVESTOR_NOT_FOUND= "Investor does not exist.";
	public static final String NOT_ELIGIBLE_FOR_WITHDRAWAL= "Please ensure that your are eligible for withdrawal.";
	public static final String INVALID_AMOUNT= "You are not allowed to withdraw more than 90% of your balance.";
	public static final String DATA_RETRIEVED_SUCCESS="Details retrieved successfully.";
	public static final String PRODUCT_NOT_FOUND="Product not found.";
	public static final String WITHDRAWAL_SUCESS="Amount has been withdrawn successfully.";
}
