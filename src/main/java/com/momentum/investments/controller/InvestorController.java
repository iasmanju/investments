package com.momentum.investments.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.momentum.investments.constants.InvestmentResponseMessages;
import com.momentum.investments.dto.CompleteInvestorDetails;
import com.momentum.investments.dto.WithdrawalRequest;
import com.momentum.investments.exceptions.DataNotFoundException;
import com.momentum.investments.exceptions.ValidationException;
import com.momentum.investments.response.APIResponse;
import com.momentum.investments.service.InvestorDetailsService;
import com.momentum.investments.service.ProductService;

@RestController
public class InvestorController {
	public static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(InvestorController.class);

	@SuppressWarnings("rawtypes")
	private APIResponse response;
	private InvestorDetailsService investmentDetailsService;
	private ProductService productService;
	
	@Autowired
	public InvestorController(InvestorDetailsService investmentDetailsService, ProductService productService) {
		this.investmentDetailsService = investmentDetailsService;
		this.productService = productService;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping("/getInvestorDetails")
	public ResponseEntity<APIResponse<CompleteInvestorDetails>> getInvestorDetails(
			@RequestParam(name = "Id") int investorId) throws Exception {
		try {
			CompleteInvestorDetails investorDetails = investmentDetailsService.getDetails(investorId);
			logger.info("Data returned from DB : " + investorDetails.toString());
			System.out.println(investorDetails);
			response = APIResponse.builder()
					.responseObject(investorDetails)
					.code(HttpStatus.OK)
					.responseMessage(InvestmentResponseMessages.DATA_RETRIEVED_SUCCESS)
					.build();
		} 
		catch (DataNotFoundException ex) {
			logger.error(ex.getLocalizedMessage());
			throw ex;

		}
		catch (Exception ex) {
			logger.error(ex.getLocalizedMessage());
			throw ex;
		}
		return new ResponseEntity<APIResponse<CompleteInvestorDetails>>(response, response.getCode());
	}

	@SuppressWarnings("unchecked")
	@RequestMapping("/withdraw")
	public ResponseEntity<APIResponse<String>> getInvestorDetails(@RequestBody WithdrawalRequest withdraw)throws Exception {
		try {
			String result = productService.withdrawAmount(withdraw);
			response = APIResponse.builder()
					.responseObject(result)
					.code(HttpStatus.OK)
					.responseMessage("SUCCESS")
					.build();
		} catch (DataNotFoundException ex) {
			logger.error(ex.getLocalizedMessage());
			throw ex;
		} catch (ValidationException ex) {
			logger.error(ex.getLocalizedMessage());
			throw ex;
		} catch (Exception ex) {
			logger.error(ex.getLocalizedMessage());
			throw ex;
		}
		return new ResponseEntity<APIResponse<String>>(response,response.getCode());
	}

}
