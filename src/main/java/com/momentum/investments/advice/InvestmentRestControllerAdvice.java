package com.momentum.investments.advice;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import com.momentum.investments.response.APIResponse;

@RestControllerAdvice
public class InvestmentRestControllerAdvice {
	public static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger("InvestmentDetailsService");
	@SuppressWarnings("unchecked")
	@ExceptionHandler(Exception.class)
	public 	ResponseEntity<APIResponse<String>> handleException(Exception ex){
		@SuppressWarnings("rawtypes")
		APIResponse exceptionResponse = APIResponse.builder()
				.responseObject(ex.getMessage())
				.code(HttpStatus.BAD_REQUEST)
				.responseMessage("FAILURE")
				.build();
		logger.error(ex.getLocalizedMessage());
		return new ResponseEntity<APIResponse<String>>(exceptionResponse, exceptionResponse.getCode());
	}
	
}
