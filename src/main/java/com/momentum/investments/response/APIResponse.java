package com.momentum.investments.response;

import org.springframework.http.HttpStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class APIResponse<T> {
	private  HttpStatus code ;
	private  String responseMessage;
	private T responseObject;
}
