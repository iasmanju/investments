package com.momentum.investments.service.impl;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.momentum.investments.constants.InvestmentResponseMessages;
import com.momentum.investments.dto.WithdrawalRequest;
import com.momentum.investments.exceptions.DataNotFoundException;
import com.momentum.investments.exceptions.ValidationException;
import com.momentum.investments.model.Product;
import com.momentum.investments.repository.ProductDetails;
import com.momentum.investments.service.ProductService;
import com.momentum.investments.utils.RequestValidator;

@Service
public class ProductServiceImpl implements ProductService {
	private ProductDetails productDetailsRepo;
	private InvestorDetailsServiceImpl investorService;
	private RequestValidator requestValidator;

	public static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ProductServiceImpl.class);

	@Autowired
	public ProductServiceImpl(ProductDetails productDetailsRepo, InvestorDetailsServiceImpl investorService,
			RequestValidator requestValidator) {
		this.productDetailsRepo = productDetailsRepo;
		this.investorService = investorService;
		this.requestValidator = requestValidator;
	}

	public ArrayList<Product> getProductDetailsForInvestor(int Id) {
		return productDetailsRepo.getProductsByInvestord(Id);
	}

	@Override
	public String withdrawAmount(WithdrawalRequest request) throws DataNotFoundException {
		int investorId = request.getInvestorId();
		int productId = request.getProductId();
		float amount = request.getAmount();
		if (!productDetailsRepo.existsById(productId)) {
			throw new DataNotFoundException(InvestmentResponseMessages.PRODUCT_NOT_FOUND);
		}
		Product product = productDetailsRepo.getById(productId);
		int age = investorService.getAgeById(investorId);
		if (requestValidator.validateWithdrawalRequest(age, product.getProductType(), amount)) {
			float balance = product.getCurrentBalance();
			float maxLimit = (int) (0.9 * balance);
			if (maxLimit < amount) {
				throw new ValidationException(InvestmentResponseMessages.INVALID_AMOUNT);
			} else {
				product.setCurrentBalance(product.getCurrentBalance() - amount);
				productDetailsRepo.save(product);
				return InvestmentResponseMessages.WITHDRAWAL_SUCESS;
			}
		} else {
			throw new ValidationException(InvestmentResponseMessages.NOT_ELIGIBLE_FOR_WITHDRAWAL);
		}
	}
}
