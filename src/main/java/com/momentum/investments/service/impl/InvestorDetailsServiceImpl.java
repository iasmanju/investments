package com.momentum.investments.service.impl;

import java.time.LocalDate;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.momentum.investments.constants.InvestmentResponseMessages;
import com.momentum.investments.dto.CompleteInvestorDetails;
import com.momentum.investments.exceptions.DataNotFoundException;
import com.momentum.investments.model.Investor;
import com.momentum.investments.model.Product;
import com.momentum.investments.repository.InvestorDetails;
import com.momentum.investments.repository.ProductDetails;
import com.momentum.investments.service.InvestorDetailsService;

@Service
public class InvestorDetailsServiceImpl implements InvestorDetailsService {
	public static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(InvestorDetailsServiceImpl.class);

	InvestorDetails investorDetailsRepo;
	ProductDetails productDetailsRepo;
	
	@Autowired
	public InvestorDetailsServiceImpl(InvestorDetails investorDetailsRepo, ProductDetails productDetailsRepo) {
		this.investorDetailsRepo =investorDetailsRepo;
		this.productDetailsRepo = productDetailsRepo;
	}

	@Override
	public CompleteInvestorDetails getDetails(int Id) throws Exception {
		if (investorDetailsRepo.existsById(Id)) {
			Investor investor = investorDetailsRepo.getById(Id);
			System.out.println(investor.getInvestorId());
			ArrayList<Product> products = productDetailsRepo.getProductsByInvestord(Id);
			CompleteInvestorDetails details = CompleteInvestorDetails.builder()
					.investor(investor)
					.products(products)
					.build();
			return details;
		} else {
			logger.info("Investor does not exist with Id " + Id);
			throw new DataNotFoundException(InvestmentResponseMessages.INVESTOR_NOT_FOUND);
		}
	}

	@Override
	public int getAgeById(int investorId) {
		if (!investorDetailsRepo.existsById(investorId)) {
			throw new DataNotFoundException(InvestmentResponseMessages.INVESTOR_NOT_FOUND);
		} else {
			Investor investor = investorDetailsRepo.getById(investorId);
			int age = LocalDate.now().compareTo(investor.getDob());
			return age;
		}
	}

}
