package com.momentum.investments.service;

import com.momentum.investments.dto.CompleteInvestorDetails;

public interface InvestorDetailsService {

	public CompleteInvestorDetails getDetails(int Id) throws Exception;
	public int getAgeById(int investorId);
}
