package com.momentum.investments.service;

import java.util.ArrayList;

import com.momentum.investments.dto.WithdrawalRequest;
import com.momentum.investments.model.Product;

public interface ProductService {
	public ArrayList<Product> getProductDetailsForInvestor(int Id);
	public String withdrawAmount(WithdrawalRequest request);
}
