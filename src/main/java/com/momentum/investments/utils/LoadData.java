package com.momentum.investments.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.momentum.investments.repository.InvestorDetails;
import com.momentum.investments.repository.ProductDetails;

@Component
public class LoadData implements CommandLineRunner {
	public static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger("LoadData");

	@Autowired
	private InvestorDetails investorDetailsRepo;

	@Autowired
	private ProductDetails productDetailsRepo;

	@Override
	public void run(String... args) throws Exception {
/*
		try {
			org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger("LoadData");
			Investor investor = new Investor();
			investor.setFirstName("test");
			investor.setLastName("test");
			investor.setEmailId("test");

			investor.setAddress("test");
			investor.setMobileNumber(new BigInteger("900"));
			investor.setDob(LocalDate.now().minusYears(68));
			investorDetailsRepo.save(investor);
			
			Investor investor2 = new Investor();
			investor2.setFirstName("test1");
			investor2.setLastName("test1");
			investor2.setEmailId("test1");
			investor2.setAddress("test1");
			investor2.setMobileNumber(new BigInteger("900"));
			investor2.setDob(LocalDate.now().minusYears(3));
			investorDetailsRepo.save(investor2);

			Product product = new Product();
			product.setCurrentBalance(100F);
			product.setInvestor(investor);
			product.setProductName("testprod");
			product.setProductType("Retirement");
			logger.info("Data has been inserted successfully");
			productDetailsRepo.save(product);
			
			Product product1 = new Product();
			product1.setCurrentBalance(100000F);
			product1.setInvestor(investor);
			product1.setProductName("testprod");
			product1.setProductType("Retirement");
			productDetailsRepo.save(product1);

			Product product3 = new Product();
			product3.setCurrentBalance(100000F);
			product3.setInvestor(investor2);
			product3.setProductName("testprod");
			product3.setProductType("Retirement");
			productDetailsRepo.save(product3);

			logger.info("Data has been inserted successfully");
		} catch (Exception e) {
			// TODO Auto-generated catch block e.printStackTrace(); }

		}*/
	}
		
}
