package com.momentum.investments.utils;

import org.springframework.stereotype.Component;

@Component
public class RequestValidator {

	public boolean validateWithdrawalRequest(int age, String productType, float amount) {
		if (productType.equals("Retirement")) {
			if (age > 65) {
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
	}
}
