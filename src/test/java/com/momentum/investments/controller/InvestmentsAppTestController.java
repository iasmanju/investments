package com.momentum.investments.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.momentum.investments.InvestmentsApplicationTests;
import com.momentum.investments.dto.CompleteInvestorDetails;
import com.momentum.investments.dto.WithdrawalRequest;
import com.momentum.investments.response.APIResponse;

public class InvestmentsAppTestController extends InvestmentsApplicationTests {

	@Override
	@Before
	public void setUp() {
		super.setUp();
	}

	@Test
	public void getCompleteInvestorDetailsPostive() throws Exception {
		//Valid investor id
		super.setUp();
		String uri = "/getInvestorDetails?Id=1";
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE))
				.andReturn();

		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
		String content = mvcResult.getResponse().getContentAsString();
		APIResponse<CompleteInvestorDetails> completeDetails = super.mapFromJson(content, APIResponse.class);
		assertEquals("Details retrieved successfully.",completeDetails.getResponseMessage());
	}
	@Test
	public void getCompleteInvestorDetailsNegative() throws Exception {
		//Invalid investor id
		super.setUp();
		String uri = "/getInvestorDetails?Id=3";
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE))
				.andReturn();

		int status = mvcResult.getResponse().getStatus();
		assertEquals(400, status);
		String content = mvcResult.getResponse().getContentAsString();
		APIResponse<CompleteInvestorDetails> completeDetails = super.mapFromJson(content, APIResponse.class);
		assertEquals(completeDetails.getResponseObject(),"Investor does not exist.");
	}

	@Test
	public void withdrawAmountPositve() throws Exception {
		//Valid amount
		super.setUp();
		String uri = "/withdraw";
		WithdrawalRequest request = new WithdrawalRequest();
		request.setAmount(50);
		request.setInvestorId(1);
		request.setProductId(1);

		String inputJson = super.mapToJson(request);
		MvcResult mvcResult = mvc.perform(
				MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
				.andReturn();

		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
		String content = mvcResult.getResponse().getContentAsString();
		APIResponse<String> response = super.mapFromJson(content, APIResponse.class);

		assertEquals("Amount has been withdrawn successfully.",response.getResponseObject());
	}

	@Test
	public void withdrawAmountNegative1() throws Exception {
		//Invalid Amount
		super.setUp();
		String uri = "/withdraw";
		WithdrawalRequest request = new WithdrawalRequest();
		request.setAmount(500);
		request.setInvestorId(1);
		request.setProductId(1);
		String inputJson = super.mapToJson(request);
		MvcResult mvcResult = mvc.perform(
				MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
				.andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(400, status);
		String content = mvcResult.getResponse().getContentAsString();
		APIResponse<String> response = super.mapFromJson(content, APIResponse.class);

		assertEquals("You are not allowed to withdraw more than 90% of your balance.",response.getResponseObject());
	}
	@Test
	public void withdrawAmountNegative2() throws Exception {
		//Invalid age
		super.setUp();
		String uri = "/withdraw";
		WithdrawalRequest request = new WithdrawalRequest();
		request.setAmount(500);
		request.setInvestorId(2);
		request.setProductId(3);
		String inputJson = super.mapToJson(request);
		MvcResult mvcResult = mvc.perform(
				MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
				.andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(400, status);
		String content = mvcResult.getResponse().getContentAsString();
		APIResponse<String> response = super.mapFromJson(content, APIResponse.class);

		assertEquals("Please ensure that your are eligible for withdrawal.",response.getResponseObject());
	}
	@Test
	public void withdrawAmountNegative3() throws Exception {
		//Invalid product
		super.setUp();
		String uri = "/withdraw";
		WithdrawalRequest request = new WithdrawalRequest();
		request.setAmount(500);
		request.setInvestorId(2);
		request.setProductId(9);
		String inputJson = super.mapToJson(request);
		MvcResult mvcResult = mvc.perform(
				MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
				.andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(400, status);
		String content = mvcResult.getResponse().getContentAsString();
		APIResponse<String> response = super.mapFromJson(content, APIResponse.class);

		assertEquals(response.getResponseObject(),"Product not found.");
	}

}
